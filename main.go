package main

import "github.com/gin-gonic/gin"

// swagger:route GET /ping repos just for test go-swagger
// Creates a new repository for the currently authenticated user.
// If repository name is "exists", error conflict (409) will be returned.
// responses:
//  200: repoResp
//  400: badReq
//  409: conflict
//  500: internal
func main() {
	route := gin.Default()
	route.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	route.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
